#include <SFML/Graphics.hpp>
#include <Fractale.h>

using namespace std;
using namespace sf;

//const int RES_X = 800;
//const int RES_Y = 600;

int main()
{
    cout << "Fractale de Mandelbrot V2" <<endl <<endl;

    sf::ContextSettings settings;
    settings.antialiasingLevel = 8;

    sf::RenderWindow window(sf::VideoMode(RES_X, RES_Y, 32), "Fractales", sf::Style::Default, settings);
    window.setFramerateLimit(60);

    Fractale f;
    f.Calculer();

    while (window.isOpen())
    {
        sf::Event event;

        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed){
                window.close();
            }
            else if(event.type == Event::MouseWheelMoved){      // zoom / dezoom avec la molette
                if(event.mouseWheel.delta > 0){
                    f.zoomIn();
                } else {
                    f.zoomOut();
                }
            }
            else if(event.type == Event::KeyPressed){           // Déplacement
                if(event.key.code == Keyboard::Escape){
                    f.reset();
                }

                if(event.key.code == Keyboard::Add){
                    f.increaseIteration();
                }
                if(event.key.code == Keyboard::Subtract){
                    f.decreaseIteration();
                }

                if(event.key.code == Keyboard::Left){
                    f.move(f.Left);
                }
                if(event.key.code == Keyboard::Right){
                    f.move(f.Right);
                }
                if(event.key.code == Keyboard::Up){
                    f.move(f.Up);
                }
                if(event.key.code == Keyboard::Down){
                    f.move(f.Down);
                }
            }
        }

        window.clear();

        window.draw(f.getSprite());
        window.draw(f.getPerformance());
        window.draw(f.getInfo());
        window.display();
    }


    return 0;
}
