#ifndef FRACTALE_H
#define FRACTALE_H

#include <SFML/Graphics.hpp>
#include <iostream>

using namespace std;
using namespace sf;

const int RES_X = 800;
const int RES_Y = 600;

typedef sf::Vector2<double>        Vector2lf;

struct Range {
    double x_min, x_max;
    double y_min, y_max;
};

class Fractale
{
    public:
        Fractale();

        void Calculer();
        void Afficher();

        void reset();

        Image getImg(){return img;}

        void setZoom(double _zoom){zoom = _zoom;}
        double getZoom(){return zoom;}

        void setMaxIteration(double _iteration){max_iteration = _iteration;}
        double getMaxIteration(){return max_iteration;}

        void setNormalizedPosition(Vector2lf vec){normalizedPosition = vec;}

        Sprite getSprite(){return sprite;}
        Text getPerformance(){return performance;}
        Text getInfo(){return info;}

        void increaseIteration();
        void decreaseIteration();

        void zoomIn();
        void zoomOut();

        enum Direction {
            Left,
            Right,
            Up,
            Down
        };

        void move(Direction aDirection);

    protected:
    private:
        double zoom;
        double max_iteration;

        double window_size_x;
        double window_size_y;

        Vector2lf normalizedPosition;


        Image img;
        Sprite sprite;
        Texture texture;

        Text performance;
        Text info;

        Font myFont;

        Clock timer;
};

#endif // FRACTALE_H
