#include "Fractale.h"
#include <sstream>

template <typename T>
std::string ftostr(const T& obj)
{
	std::ostringstream ss;
	ss << obj;
	return ss.str();
}

Fractale::Fractale() :
zoom(1.),
max_iteration(40),
normalizedPosition(0.4, 0.5),
window_size_x(RES_X),
window_size_y(RES_Y)
{
    img.create(window_size_x, window_size_y, Color::Black);

    myFont.loadFromFile("sansation.ttf");

    performance.setCharacterSize(14);
    performance.setFont(myFont);
    performance.setColor(Color::White);
	performance.setString("Fractal rendered in 0 ms");
    performance.setPosition(window_size_x - performance.getGlobalBounds().width  -10, 10);

    info.setCharacterSize(14);
    info.setFont(myFont);
    info.setColor(Color::White);
    info.setString("Zoom : x" + ftostr(zoom) + "\n" +
                   "Iterations : " + ftostr(max_iteration) + "\n" +
                   "Position : " + ftostr(normalizedPosition.x) + " ; " + ftostr(normalizedPosition.y));
    info.setPosition(10, window_size_y - info.getGlobalBounds().height -15);
}

void Fractale::increaseIteration() {
    setMaxIteration(getMaxIteration() + 10);
    Calculer();
}

void Fractale::decreaseIteration() {
    if(getMaxIteration() - 10 < 0){
        setMaxIteration(getMaxIteration());
    } else {
        setMaxIteration(getMaxIteration() - 10);
    }
    Calculer();
}

void Fractale::zoomIn() {
    setZoom(getZoom() * 2);
    Calculer();
}

void Fractale::zoomOut() {
    setZoom(getZoom() * 1/2);
    Calculer();
}

void Fractale::move(Direction aDirection){
    Vector2lf position = normalizedPosition;
    double offset = .1 / zoom;

    switch (aDirection) {
		case Left:	position.x -= offset;	break;
		case Right:	position.x += offset;	break;
		case Up:	position.y -= offset;	break;
		case Down:	position.y += offset;	break;
		default:	break;
	}

    setNormalizedPosition(position);
    Calculer();
}

void Fractale::reset() {
    setNormalizedPosition(Vector2lf(0.4, 0.5));
    setZoom(1.);
    setMaxIteration(40);
    Calculer();
}

void Fractale::Calculer(){
    cout << "Calculating ... ";
    timer.restart();

    const double fractale_left = -2.1;
	const double fractale_right = 0.6;
	const double fractale_bottom = -1.2;
	const double fractale_top = 1.2;

	double zoom_y = zoom * window_size_y / (fractale_top - fractale_bottom);
	double zoom_x = zoom_y;

	int64_t fractale_width = window_size_x * zoom;
	int64_t fractale_height = window_size_y * zoom;

    for(double x=0; x <window_size_x; x++){
        for(double y=0; y<window_size_y; y++){

            double z_r = 0;
            double z_i = 0;

            int64_t fractale_x = fractale_width * normalizedPosition.x - window_size_x / 2 + x;
			int64_t fractale_y = fractale_height * normalizedPosition.y - window_size_y / 2 + y;

//            double c_r = zoom*(fractale_right-fractale_left)*x/window_size_x + fractale_left;
//            double c_i = zoom*(fractale_top-fractale_bottom)*y/window_size_y+ fractale_bottom;

            double c_r = fractale_x / zoom_x + fractale_left;
            double c_i = fractale_y / zoom_y + fractale_bottom;

            double i = 0;

            while(z_r*z_r + z_i*z_i < 4. && i < max_iteration){
                double tmp = z_r;
                z_r = (z_r*z_r - z_i*z_i + c_r);
                z_i = (2*z_i*tmp + c_i);
                i++;
            }
            if( i != max_iteration){
                int tmp = i*255/max_iteration;
                img.setPixel(x,y,Color(0,tmp/2,tmp));
            }
            if(i == max_iteration) {
                img.setPixel(x,y,Color::Black);
            }
        }
    }

    performance.setString("Fractal rendered in " + ftostr(timer.getElapsedTime().asMilliseconds())+ " ms");
    performance.setPosition(window_size_x - performance.getLocalBounds().width - 10, 10);

    info.setString("Zoom : x" + ftostr(zoom) + "\n" +
                   "Iterations : " + ftostr(max_iteration) + "\n" +
                   "Position : " + ftostr(normalizedPosition.x) + " ; " + ftostr(normalizedPosition.y));
    info.setPosition(10, window_size_y - info.getGlobalBounds().height -15);

    Afficher();

    cout << "Done" <<endl;
}


void Fractale::Afficher(){
    texture.loadFromImage(getImg());
    sprite.setTexture(texture);
}
